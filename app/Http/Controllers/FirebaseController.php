<?php  
 namespace App\Http\Controllers;  
 use Illuminate\Http\Request;  
 use Kreait\Firebase;  
 use Kreait\Firebase\Factory;  
 use Kreait\Firebase\ServiceAccount;  
 use Kreait\Firebase\Database;  

 class FirebaseController extends Controller  
 {    

    public function makeRegister(Request $request)
    {
         $this->validate($request, [
            'name' => 'required|min:4',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6|confirmed'
      ]);

        $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/covid19tracker-33375-firebase-adminsdk-fh0jt-5caeea921a.json');  
        $firebase = (new Factory)  
        ->withServiceAccount($serviceAccount)  
        ->withDatabaseUri('https://covid19tracker-33375-default-rtdb.firebaseio.com')  
        ->create();  
        $database = $firebase->getDatabase();  
        $newPost = $database  
        ->getReference('user')  
        ->push([  
        'name' => $request->name,  
        'email' => $request->email,
        'password' => bcrypt($request->password)
        ]);

        return redirect()->route('home');
    }

    public function login(Request $request)
    {

        $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/covid19tracker-33375-firebase-adminsdk-fh0jt-5caeea921a.json');  
        $firebase = (new Factory)  
        ->withServiceAccount($serviceAccount)  
        ->withDatabaseUri('https://covid19tracker-33375-default-rtdb.firebaseio.com')  
        ->create();  
        $database = $firebase->getDatabase();  
        $newPost = $database  
        ->getReference('user')  
        ->pull([
        'email' => $request->email,
        'password' => bcrypt($request->password)
        ]);

        return redirect()->route('home');
    }
 }  
 ?>  