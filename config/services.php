<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    'firebase' => [
        'api_key' => 'AAAA01uvSS0:APA91bH7HJQkEa1Bf0YjbaT7sEaFBvGTS4RZ_8JUv5D9ScSvIRCqgScLi0Dw95Z8n3RwBNSDAKZekM568DRDKhiC_Ta4MnvFiSgK-wvEOhRP7X9_6BHpmhWHdwWQnwI0mF-V7PnaY1dE',
        'auth_domain' => 'covid19tracker-33375.firebaseapp.com',
        'database_url' => env('FIREBASE_DATABASE_URL', 'https://covid19tracker-33375-default-rtdb.firebaseio.com'),
        'project_id' => env('FIREBASE_PROJECT_ID', 'covid19tracker-33375'),
        'private_key_id' => env('FIREBASE_PRIVATE_KEY_ID', '5caeea921ad4a8d7536606b4c4afde0b4a5d491c'),
        'storage_bucket' => 'gs://covid19tracker-33375.appspot.com',
        'messaging_sender_id' => '907776313645',
        // replacement needed to get a multiline private key from .env 
        'private_key' => str_replace("\\n", "\n", env('FIREBASE_PRIVATE_KEY', '-----BEGIN PRIVATE KEY-----\nMIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQCkPRrxmI5BXpyN\notk9ves3rnEmFc0wUz0R1+UAJAUBwDDJZBaG1Ys64uk6ZleL5edL0CT1+5k8pUWk\nZnPTjNYQVK2a2K94vhul+g8S5iVkDuHb0PmePSQnss4xraBIykd9zVGERzhQMpAk\n0+fo8FU7pmddzkGXHDZqwP0zHnbl3Lj65pzDtBakeRQVPtsx/gtLhpyzDXc5T1fE\nglsEZo4bLcLRnsUYAGY6k0mb1bhCICf7q0EE8hl8ItUEiyBfRTIRk7BYsIN0JVtU\ngxgkmaY3x6YxIwsN+rtQ5Q/j5hNLTfkUYqR82hCmp+aGpF8+K8m88NtF51v6D3yh\nvHu9Gfb7AgMBAAECggEAT4aNBN33R9zdllF7uoM7IZeUjznwRI2RnRG6f3JxgDMF\nDIcrxzcgdy+HejEzA/kbIaEUPI2K2gw26A951z4zKu4tr7RrZt46uggWg3KwOT5h\ni9QpAj+dwaE5aWGmoohbzKT96zynHmIVXNfEzAyPYNZG5+um49oT2WZAVB/Ut6D9\nYYX/avvIafFpt7F1CQDHwsDc10iEDb2gS9UjpvZn7TNwFMoFh0wO/0hsNO76+MtU\nSMPP4r25qj1z+Qy/uKe09yz8tVBX7d1SNyTYfyszHrALaY8TAYqyBp2gIFe5QJ3e\noKKKZb9kxFTnDQ/bXHp4wNps6WkIenMWIL/ll9lHqQKBgQDbgotNv/+O4K2/GbY+\nYNeTw9lkKbv68ELxsrDkWteXUKryy05HaRYDabYGw06CgWgrjX9kg40T+qU2/5ug\ndtqOJ91IP4lOxKNLgSW5Oh45a1NGJubuPOaVCVwlrbNABGVH2zGW7PSszMH+Xa/2\nGI4e0WS9IKI77AioG+sgV4um0wKBgQC/inC0C0Q9b51V6uCl3Vp2OhPQg04s25l4\nIeJw59Qd6HD7WgfiO+7r4fWVcV4WmFojzjbkWrlCgplWowOnP48SGoLlAhOYt4yM\na2gN+1dJJIGvaYQ1XkXiF9QL8eUX2nb/3cbmkjFe5m7gYzVnlU6yNMio6yqkPFoa\nYb/M0LemOQKBgQCiws8r3VtOw8XQ1u3Mnt36ENQWcuORk6jvskF7cOg+qG+5C4qF\nZKQP78gqh5KlLfLhqE93ZhurXsQ2XZ01hCtWELUBJytWnnM+PwqLp05V4QCkGaBN\nshZ/F6udvIgWxaubdin08MoTftexFiKs5VQsAbMikRn6OG/Kxri3CHLiRQKBgQCw\nG/jOAcHnftU5pvCQcHp5U6LeR6w8IFj2FKmfx/fG2+3tnPS8tQBTD3n+wrKQsdnH\nLBZaVXQay/typgV45D6GlaueJg+gjbsDbMnKeTri/Fbk0m6Fpz03yVLk/K74BJz/\nWolDfE06hUKSu4ThzW0C+vc4IKkfQoJR2WhBR/JjOQKBgQCcSCVoVcolYn3X8pEI\ngOVeUlGtHx4PPFjc7yivBgllC41OZ86KhE3zKkaT02lIbWbdftKH7WUR8OhNaeh9\nRZ5BRKRdnzZg1GMJ5yoDiRkS3V8lYEzDwdrlJezdDKsSP7pF+EFKeznzWT3YU9g7\nghNrLEH+WQ5ly0Ziz+4b9bUM+A==\n-----END PRIVATE KEY-----\n')),
        'client_email' => env('FIREBASE_CLIENT_EMAIL', 'firebase-adminsdk-fh0jt@covid19tracker-33375.iam.gserviceaccount.com'),
        'client_id' => env('FIREBASE_CLIENT_ID', '102091344666370121179'),
        'client_x509_cert_url' => env('FIREBASE_CLIENT_x509_CERT_URL', 'https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-fh0jt%40covid19tracker-33375.iam.gserviceaccount.com'),
    ],

];
